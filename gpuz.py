
import json
import puz
import html
import re
import urllib.request
import pyquery
import sys

if len(sys.argv) < 3:
    print("Usage: ./gpuz.py <latest cw number> <count to go back>")
    exit()

latest = int(sys.argv[1])
count_back = int(sys.argv[2])

PUZ_URL = "https://www.theguardian.com/crosswords/cryptic/{}"
DEFAULT_NOTE = "Created by gpuz.py"
CW_NAME = "Guardian_{}"

ASCII_REPLACEMENTS = [
    ("&#x27;", "'"),
    ("\u2026", "..."),
    ("\u2014", "--")
]


def get_json(num):
    url = PUZ_URL.format(num)
    contents = urllib.request.urlopen(url).read()
    pq = pyquery.PyQuery(contents)
    cw = pq('.js-crossword')
    json_string = cw.attr("data-crossword-data")
    return json.JSONDecoder().decode(s=json_string)

def get_solution(dj):
    solution = [ [ '.' for i in range(15) ]
                 for j in range(15) ]

    for entry in dj["entries"]:
        x = int(entry["position"]["x"])
        y = int(entry["position"]["y"])
        word = entry["solution"]

        dx = 0
        dy = 0
        if entry["direction"] == "across":
            dx = 1
        else:
            dy = 1

        for c in word:
            solution[y][x] = c
            x += dx
            y += dy

    solution_str = ""

    for row in solution:
        for c in row:
            solution_str += c

    return solution_str

def get_fill(solution):
    return re.sub(r"[^.]", "-", solution)

def ascii_clue(clue):
    for left, right in ASCII_REPLACEMENTS:
        clue = clue.replace(left, right)
    return html.unescape(clue)

def get_clues(dj):
    across = {}
    down = {}
    nums = set()

    for entry in dj["entries"]:
        num = int(entry["number"])
        nums.add(num)

        clue = ascii_clue(entry["clue"])

        if entry["direction"] == "across":
            across[num] = clue
        else:
            down[num] = clue

    clues = []
    for num in sorted(nums):
        if num in across:
            clues.append(across[num])
        if num in down:
            clues.append(down[num])

    return clues

def write_puz(name, json_data):
    cw = puz.Puzzle()

    cw.title = json_data["name"]
    cw.author = json_data["creator"]["name"]
    cw.copyright = "Guardian / " + cw.author

    # always the same
    cw.width = 15
    cw.height = 15

    cw.solution = get_solution(json_data)
    cw.fill = get_fill(cw.solution)
    cw.clues = get_clues(json_data)

    cw.notes = DEFAULT_NOTE

    cw.save(name + ".puz")

for num in range(latest, latest - count_back, -1):
    try:
        print("Getting {}".format(num))
        cw_json = get_json(num)
        write_puz(CW_NAME.format(num), cw_json)
    except:
        print("Could not get {}".format(num))
