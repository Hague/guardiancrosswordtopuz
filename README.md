# Guardian Crossword to Puz Format

Very rough and ready script for downloading Guarding cryptic crosswords
and generating .puz files.

## Requires

* [puzpy](https://pypi.org/project/puzpy/)
* [pyquery](https://pypi.org/project/pyquery/)

## Usage

Run

    $ python gpuz.py <latest number> <count back>

where

* `<latest number` is the most recent crossword number to fetch, and
* `<count back>` is how many crosswords back to try.

Saves files as `Guardian_<num>.puz` in the directory you ran the script.

## Supporting The Guardian

You may [consider a contribution][contrib] to The Guardian for your
crosswords.

[contrib]: https://support.theguardian.com/us/contribute
